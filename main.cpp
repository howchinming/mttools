#include <iostream>
// #include <vector>
// #include <string>

#include "mtnumeral.h"
#include "mtalgorithm.h"
#include "mtstrmanip.h"

using namespace std;

int main()
{
    vector<uint64_t> primes(mtt::findPrimes(10000));

    for (vector<uint64_t>::const_iterator citr(primes.begin());
         citr != primes.end();
         advance(citr, 1))
    {
        cout << *citr << endl;
    }

    cout << endl;
    cout << "total : " << primes.size() << endl;
    cout << endl;
}
