#ifndef __MTTOOLS_MTNUMERAL_H__
#define __MTTOOLS_MTNUMERAL_H__

#include <cstdlib>
#include <cmath>

#include "mtdefine.h"
#include "mtalgorithm.h"

BEGIN_NS(mtt)

template <typename T, int32_t base>
class digit
{
    public:
        digit(const T& val=0)
        : _base(std::abs(base))
        , _val(val)
        , _res(0)
        , _cy(0)
        {
            static_assert(base > 0, "invalid base for mtt::digit");
            operator=(val);
        }

        ~digit() {}

        /// copy constructor
        digit(const digit& copy)
        : _base(copy._base)
        , _val(copy._val)
        , _res(copy._res)
        , _cy(copy._cy)
        {}

        digit & operator=(const digit& rhs)
        {
            if (this != &rhs)
            {
                // NOTE: template will ensure same base.
                _val = rhs._val;
                _res = rhs._res;
                _cy = rhs._cy;
            }

            return *this;
        }

        digit & operator=(const T& val)
        {
            zerocarry();
            _res = _val = val;
            while (_res >= _base)
            {
                _res = _res - _base;
                ++_cy;
            }
            while (_res < 0)
            {
                _res = _res + _base;
                --_cy;
            }
            
            return *this;
        }

        /// @note a digit type can be used as 
	    /// the basic type transparently
//	    operator T() const	{ return _val; }    ///< disabled to prevent unintended behaviour/usage @TODO:

        const T& value() const { return _val; }
        const T& final() const { return _res; }
        const T& carry() const { return _cy; }
        const T& getbase() const { return _base; }

        void zerocarry() { _cy = 0; _val = _res; }

    private:
        const T _base;

        T _val;
        T _res;
        int32_t _cy;
};

class rational
{
    public:
        rational()
        : _numer(0)
        , _denom(1)
        {}

        rational(const int64_t &num)
        : _numer(num)
        , _denom(1)
        {}

        rational(const int64_t &num, const uint64_t &den)
        : _numer(num)
        , _denom(den)
        {}

        ~rational() {}

        rational operator+(const rational &opr) const
        {
            std::vector<uint64_t> denoms(2);
            denoms[0] = _denom;
            denoms[1] = opr.denominator();
            uint64_t lcm(mtt::lcm(denoms));

            int64_t numer((lcm/_denom)*_numer);
            int64_t oprNumer((lcm/opr.denominator()) * opr.numerator());

            rational result(numer+oprNumer, lcm);
            result.reduce();
            return result;
        }

        rational operator-(const rational &opr) const
        {
            return this->operator+(rational(-opr.numerator(), opr.denominator()));
        }

        rational operator*(const rational &opr) const
        {
            rational result(_numer * opr.numerator(), _denom * opr.denominator());
            result.reduce();
            return result;
        }

        rational operator/(const rational &opr) const
        {
            // @note transfer sign because denominator is always unsigned
            rational invOpr(opr);
            invOpr.invert();
            return this->operator*(invOpr);
        }

        static rational approximate(const double &fval, const double &err)
        {
            int32_t lonum(0), loden(1), hinum(1), hiden(1);
            int32_t in(fval);
            double rval(fval - (double)in);
            int32_t tgtnum(lonum+hinum), tgtden(loden+hiden);
            double tgtval((double)tgtnum/(double)tgtden);
            while (std::abs(rval-tgtval) > err)
            {
                if (rval > tgtval)
                {
                    lonum = tgtnum;
                    loden = tgtden;
                }
                else
                {
                    hinum = tgtnum;
                    hiden = tgtden;
                }

                tgtnum = lonum+hinum;
                tgtden = loden+hiden;
                tgtval = ((double)tgtnum/(double)tgtden);
            }

            return rational(((in*tgtden) + tgtnum), tgtden);
        }


        bool isNAN() const { return !_denom; }
        void reduce()
        {
            std::vector<uint64_t> argsVec(2);
            argsVec[0] = _numer;
            argsVec[1] = _denom;
            uint64_t hcf(mtt::hcf(argsVec));
            _numer = _numer/hcf;
            _denom = _denom/hcf;
        }
        void invert()
        {
            bool isNeg(_numer < 0);
            _denom = isNeg ? -(_numer) : _numer;
            _numer = isNeg ? -(_denom) : _denom;
        }

        double value() const { return (double)_numer/(double)_denom; }
        int64_t numerator() const { return _numer; }
        uint64_t denominator() const { return _denom; }

    private:

        int64_t _numer;
        uint64_t _denom;
};

END_NS(mtt)

#endif // __MTTTOOLS_MTNUMERAL_H__
