#ifndef __MTTOOLS_MTOBJ_H__
#define __MTTOOLS_MTOBJ_H__

#include <cstdlib>

#include <chrono>

#include "mtdefine.h"

BEGIN_NS(mtt)

class ttl
{
public:

    enum 
    {
        PERSISTANT = 0,
    };

    /// @brief  Constructor
    /// @param dur @note set 0 to persist forever
    ttl(double dur)
    : _st(std::chrono::system_clock::now())
    , _ttl(dur)
    , _expired(false)
    {}
    ~ttl() {}

    void reset()
    {
        _st = std::chrono::system_clock::now();
        _expired = false;
    }

    bool expired() const 
    {
        if (!_ttl)
            return false;

        if (_expired)
            return true;

        std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
        std::chrono::duration<double> diff = now - _st;
        _expired = (diff.count() > _ttl) ? true : false;
        return _expired;
    }

private:
    std::chrono::time_point<std::chrono::system_clock> _st;
    double  _ttl;
    mutable bool _expired;      ///< optimization
};

class ttlop : public ttl
{
public:
    ttlop(double val, double ttl = ttl::PERSISTANT)
    : ttl(ttl)
    , _val(val)
    {}
    
private:
    double _val;
};

END_NS(mtt)

#endif //__MTTOOLS_MTOBJ_H__