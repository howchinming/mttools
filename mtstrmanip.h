#ifndef __MTTOOLS_MTOBJ_H__
#define __MTTOOLS_MTOBJ_H__

#include <sstream>

#include "mtdefine.h"

BEGIN_NS(mtt)

template<typename E>
void trimLeadWhiteSpace(
	std::basic_string<E> & str
)
{
	E srch[] = {' ','\r','\n','\t',0};
	size_t cnt(str.find_first_not_of(srch));
	if (cnt==std::basic_string<E>::npos)
	{
		E _empty[] = {0};
		str = _empty;
		return;
	}
	str = str.substr(cnt);
}
template<typename E>
std::basic_string<E> trimLeadWhiteSpace(
	const E * pbuf
)
{
	std::basic_string<E> strRet(pbuf);
	trimLeadWhiteSpace(strRet);
	return strRet;
}


template<typename E>
void trimTrailWhiteSpace(
	std::basic_string<E> & str
)
{
	E srch[] = {' ','\r','\n','\t',0};
	size_t len(str.find_last_not_of(srch)+1);
	str = str.substr(0,len);
}
template<typename E>
std::basic_string<E> trimTrailWhiteSpace(
	const E * pbuf
)
{
	std::basic_string<E> strRet(pbuf);
	trimTrailWhiteSpace(strRet);
	return strRet;
}

template<typename E>
void trimQuotes(
	std::basic_string<E> & str
)
{
	if (str.length()<2)
		return;
	E c(str[0]);
	if (c!=str[str.length()-1])
		return;
	if (c=='\'' ||
		c=='\"')
		str = str.substr(1,str.length()-2);
}
template<typename E>
std::basic_string<E> trimQuotes(
	const E * pbuf
)
{
	std::basic_string<E> strRet(pbuf);
	trimQuotes(strRet);
	return strRet;
}

END_NS(mtt)

#endif // __MTTOOLS_MTOBJ_H__