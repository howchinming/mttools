#ifndef __MTTOOLS_MTALGORITHM_H__
#define __MTTOOLS_MTALGORITHM_H__

#include <vector>
#include <cmath>
#include <algorithm>

#include "mtdefine.h"

BEGIN_NS(mtt)

// Functor encapsulating the Density-Based Scan clustering algorithm.
template<typename T>
class DBScan
{
    typedef typename std::vector<T> obj_vec;
    typedef typename obj_vec::const_iterator obj_citr;
    typedef typename std::vector<obj_citr> citr_vec;
    // pointer to function that compares 2 instances of type T and returns true 
    // if the objects are supposed to be collated and false otherwise.
    typedef bool(*pfn_collate)(const T & src, const T & tgt);

public:

    DBScan(const obj_vec & input, pfn_collate fnCollate)
    : _input(input)
    , _fn(fnCollate)
    {}

    ~DBScan() {}

    std::vector<obj_vec> operator() (int density=2) const
    {
        std::vector<obj_vec> result;
        if (density<2)
            return result;

        std::vector<citr_vec> _result;
        citr_vec scanned;
        for (obj_citr citr(_input.begin());
             citr != _input.end();
             std::advance(citr,1))
        {
            if (exists(scanned, citr))
                continue;

            citr_vec curr;      ///< current cluster
            citr_vec nbors(findnbors(*citr, _input, scanned));
            if (nbors.size() >= density)
            {
                size_t idx(0);
                while (nbors.size() > idx)
                {
                    if (!exists(scanned, nbors[idx]))
                    {
                        citr_vec nborsOfNbor(findnbors(*(nbors[idx]), _input, scanned));
                        if (nborsOfNbor.size() >= density)
                        {
                            curr.push_back(nbors[idx]);
                            merge(nbors, nborsOfNbor);
                        }
                        else
                        {
                            scanned.push_back(nbors[idx]);
                        }
                    }

                    ++idx;
                }
            }
            else
            {
                scanned.push_back(citr);
            }

            if (curr.size() >= density)
            {
                _result.push_back(curr);
                merge(scanned, curr);
            }
        }

        result.reserve(_result.size());
        for (typename std::vector<citr_vec>::const_iterator citr(_result.begin());
             citr != _result.end();
             advance(citr, 1))
        {
            obj_vec cluster;
            cluster.reserve(citr->size());
            for (typename citr_vec::const_iterator in_citr(citr->begin());
                 in_citr != citr->end();
                 advance(in_citr,1))
            {
                cluster.push_back(*(*in_citr));
            }
            result.push_back(cluster);
        }

        return result;
    }

private:

    obj_vec _input;
    pfn_collate _fn;

    citr_vec findnbors(const T & tgt,
                       const obj_vec & input,
                       const citr_vec & excl) const
    {
        citr_vec nbors;
        for (obj_citr citr(input.begin());
             citr != input.end();
             std::advance(citr,1))
        {
            if (exists(excl, citr))
                continue;

            if (_fn(tgt, *citr))
                nbors.push_back(citr);
        }

        return nbors;
    }

    void merge(citr_vec & src, const citr_vec & ext) const
    {
    	for (typename citr_vec::const_iterator citr(ext.begin());
             citr != ext.end();
             std::advance(citr, 1))
        {
            if (std::find(src.begin(), src.end(), *citr) != src.end())
                continue;

            src.push_back(*citr);
        }
    }

    bool exists(const citr_vec & container,
                const obj_citr & elem) const
    {
        for(typename citr_vec::const_iterator citr(container.begin());
            citr != container.end();
            ++citr)
        {
            if (elem == *citr)
                return true;
        }
        return false;
    }
};

// @note: Miller-Rabin Primality Test 
bool isPrime(uint64_t tst)  
{
    const uint64_t a[12] = {2,3,5,7,11,13,17,19,23,29,31,37};
    // simple initial optimizations
    for (int i(0); i<12; ++i) 
    {
        /// @note further optimizations
        if (tst == a[i])
            return true;
        if (tst % a[i] == 0)
            return false;
    }

    // Miller-Rabin Primality Test
    // 2,3 for numbers below 1,373,653
    // 31,73 for numbers below 9,080,191
    // 2,3,5 for numbers below 25,326.001
    // 2,3,5,7,11 for numbers below 2,152,302,898,747
    // 2,13,23,1662803 for numbers below 1,122,004,669,633
    // 2,3,5,7,11,13,17,19,23,29,31,37 for numbers below 318,665,857,834,031,151,167,461

    uint64_t d = tst - 1;
    uint64_t s = 0;
    while (!(d & 0x0000000000000001)) {
        d = d >> 1;
        ++s;
    }

    for (int i(0); i<12; ++i) 
    {
        bool probPrime(false);
        for (int j(0); j<s; ++j)
        {
            uint64_t res(a[i]);
            /// @note pow(a[i],d) mod tst == pow(a[i] mod tst, d)
            for (int k(1); k<(d*std::pow(2,j)); ++k)
                res = (res * a[i]) % tst;  

            if (res == 1 || res == tst-1)
            {
                probPrime = true;
                break;
            }
        }

        if (!probPrime)
            return false;
    }

    return true;
}

// Returns all prime numbers up to and including ceiling
std::vector<uint64_t> findPrimes(uint64_t ceiling)
{
    std::vector<uint64_t> primes(1, 2);
    if (ceiling > 1) 
    {
        int swp(3);
        while (swp <= ceiling)
        {
            bool isOddPrime(true);
            // only test through previously found prime numbers
            for (std::vector<uint64_t>::const_iterator citr(primes.begin());
                 citr != primes.end();
                 std::advance(citr, 1))
            {
                // optimization for large ceiling
                if ((double)(*citr) > sqrt((double)swp)) 
                    break;

                if (!(swp % (*citr)))
                {
                    isOddPrime = false;
                    break;
                }
            }

            if (isOddPrime)
                primes.push_back(swp);

            ++swp;
        }
    }

    return primes;
}

uint64_t hcf(const std::vector<uint64_t> & vals)
{
    uint64_t hcf_(1);
    if (!vals.size())
        return hcf_;

    uint64_t maxVal(*(std::max_element(vals.begin(), vals.end())));
    std::vector<uint64_t> primes(findPrimes(maxVal));
    for (std::vector<uint64_t>::const_iterator citr_p(primes.begin());
         citr_p != primes.end();
         std::advance(citr_p, 1))
    {
        bool isCommFac(true);
        while (isCommFac)
        {
            for (std::vector<uint64_t>::const_iterator citr_val(vals.begin());
                citr_val != vals.end();
                std::advance(citr_val, 1))
            {
                if (((*citr_val)/hcf_) % *citr_p)
                {
                    isCommFac = false;
                    break;
                }
            }

            if (isCommFac)
                hcf_ = hcf_ * (*citr_p);
        }
    }

    return hcf_;
}

uint64_t lcm(const std::vector<uint64_t> & vals)
{
    uint64_t lcm_(*(vals.begin()));
    for (std::vector<uint64_t>::const_iterator citr(vals.begin());
         citr != vals.end();
         std::advance(citr, 1))
    {
        if (citr == vals.begin())
            continue;

        std::vector<uint64_t> vec(2);
        vec[0] = lcm_;
        vec[1] = *citr;

        uint64_t hcf(mtt::hcf(vec));
        lcm_ = lcm_ * ((*citr)/hcf);
    }

    return lcm_;
}

END_NS(mtt)

#endif // __MTTOOLS_MTALGORITHM_H__